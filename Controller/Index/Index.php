<?php

namespace Sanipex\Import\Controller\Index;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;



class Index extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface {

/**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
    public function execute() {
        
        $functions = [
            "CREATE_OPTIONS" => "createAttributeOptions",
            "GET_ATTRIBUTES" => "getAttributeList",
            "GET_SKU_LIST" => "getSkuList",
            "CREATE_PRODUCTS" => "createEntities",
            "UPDATE_ATTRIBUTE" => "updateAttributes",
            "GET_STATUS" => "getStatus",
        ];
        
        $params = $this->getRequest()->getPost();
        //Database 
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $call = $functions[$params['method']];
        $response = $this->$call($connection, $params);
        echo json_encode($response);exit();
    }
    private function deleteUnassignedOptions($connection){
        $query = "delete from eav_attribute_option where option_id not in (select option_id from eav_attribute_option_value)";
        $connection->query($query);
    }
    private function createAttributeOptions($connection, $params){
        $this->deleteUnassignedOptions($connection);
        $data = json_decode($params['data'],true);
        $values = [];
        $optionString = "";
        
        foreach($data as $attributeId => $options){
            $response = $this->checkOptionExistence($connection, $attributeId, $options);
            $removedExisitingOptions = array_diff($options, $response);
            if(count($removedExisitingOptions) > 0){
                foreach($removedExisitingOptions as $option){
                    $values[$attributeId][] = $option;
                    $optionString.=",($attributeId,0)";
                }
            }
        }
        if($optionString != ""){
            $this->createOptions($connection, $optionString);
            $response = $this->mappOptionsValues($connection, $values);
            $optionValuesString  = $response['query'];
            try{
                $this->insertOptionValues($connection, $optionValuesString);
                $this->insertSwatchOptions($connection, $response['optionMappedArr']);
                return ["error" => false , 'response'=> "Options created successfully!"];
            } catch(Exception $e){
                return ['response'=> "Error in  creating options!"];
            }
        }
        return ["error" => false , 'response'=> "No Options to create"];
    }
    
    private function insertSwatchOptions($connection, $options){
        $optionIDs = array_keys($options);
        $values = "";
        $query = "insert into eav_attribute_option_swatch (option_id,store_id,type,value) values ";
        foreach($optionIDs as $optionId){
            $values .= ",($optionId,0,3,NULL)";
        }
        $query .= ltrim($values, ',');
        $connection->query($query);
    }
    private function insertOptionValues($connection, $optionValuesString){
        $query = "INSERT INTO eav_attribute_option_value (option_id,store_id, value) VALUES ";
        $query.=ltrim($optionValuesString, ',');
        return $connection->query($query);
    }
    
    private function mappOptionsValues($connection, $optionValues){
        $mappedOptionQueryValues = "";
        $query = "select option_id, attribute_id from eav_attribute_option where option_id not in (select option_id from eav_attribute_option_value)";
        $response = $connection->fetchAll($query);
        
        $reformedOptionIds = [];
        foreach($response as $optionId){
            $reformedOptionIds[$optionId['attribute_id']][] = $optionId['option_id'];
        }
        $optionArr = [];
        foreach($reformedOptionIds as $attributeId => $values){
            $i = 0;
            foreach($values as $optionId ) {
                $optionValue = $optionValues[$attributeId][$i];
                $optionArr[$optionId] = $optionValue;
                $mappedOptionQueryValues .= ",($optionId, 0, '".addslashes($optionValue)."')";
                $i++;
            }
        }
        $response['query'] = $mappedOptionQueryValues;
        $response['optionMappedArr'] = $optionArr;
        return $response;
    }
    
    private function createOptions($connection, $optionString){
        $query = "INSERT INTO eav_attribute_option (attribute_id,sort_order) VALUES ";
        $query.=ltrim($optionString, ',');
        $connection->query($query);
    }
    
    private function checkOptionExistence($connection, $attributeId, $options){

        $query = "Select option_id,value from eav_attribute_option_value where option_id in (SELECT option_id from eav_attribute_option where attribute_id = $attributeId);"; 
        $return = $connection->fetchAll($query);
        $response = [];
        $reformed = [];
        foreach ($return as $option){
            $reformed[$option['option_id']] = $option['value'];
        }
        foreach($reformed  as $optionId => $optionValue){
            if(in_array($optionValue,$options)){
                $response[$optionId] = $optionValue;
            }
        }
        return $response;
    }
    private function getAttributeList($connection, $params){
        $query = "Select attribute_id, attribute_code, backend_type from eav_attribute where entity_type_id = 4";
        
        $query1 = "select eao.attribute_id , eao.option_id, eaov.`value` from eav_attribute_option eao inner join eav_attribute_option_value eaov on eaov.`option_id` = eao.`option_id`;";
        $data = $connection->fetchAll($query);
        $data1 = $connection->fetchAll($query1);
        $response = [];
        $options = [];
        foreach($data1 as $attribute){
            $options[$attribute['attribute_id']][$attribute['option_id']] = $attribute['value'];
            
        }
        
        foreach($data as $attribute){
            $response[$attribute['attribute_id']]['attribute_code'] = $attribute['attribute_code'];
            $response[$attribute['attribute_id']]['backend_type'] = $attribute['backend_type'];
            if($attribute['backend_type'] == 'int'){
                if(isset($options[$attribute['attribute_id']])){
                    $response[$attribute['attribute_id']]['options'] = $options[$attribute['attribute_id']];
                }else {
                    $response[$attribute['attribute_id']]['options'] = [];
                }
            }
        }
        return $this->prepareResponse($response);
    }

    private function createEntities($connection, $params){
        $parentSkus = [];
        $allSkus = [];
        $childSkus = [];
        $configrations = [];
        $parentChildArr = [];
        $data = json_decode($params['data'],true);
        
        foreach($data as $parent => $product){
            $parentSkus[] = $parent;
            $allSkus[] = $parent;
            foreach($product['children'] as $children){
                $allSkus[] = $children['sku'];
                $childSkus[] = $children['sku'];
            }
        }
        
        if(count($data) > 0){
            $entitiesExists = $this->getEntityIds($connection, $allSkus);
            $this->createEntites($connection, $data,$entitiesExists,$params['attribute_set_id']);
            $entities = $this->getEntityIds($connection, $allSkus);
            $exisitngStores = $this->checkStore($connection,$data);
            //values
            $varcharValues = ltrim($this->processAttributes($connection, $data, $entities, 'varchar'), ',');
            $intValues = ltrim($this->processAttributes($connection, $data, $entities, 'int'), ',');
            $decimalValues = ltrim($this->processAttributes($connection, $data, $entities, 'decimal'), ',');
            $textValues = ltrim($this->processAttributes($connection, $data, $entities, 'text'), ','); 
            $this->insertStoreStatus($connection,$data,$entities,$exisitngStores);
            //insert Queries
            $this->insertAttributes($connection,$varcharValues,'varchar');
            $this->insertAttributes($connection,$intValues,'int');
            $this->insertAttributes($connection,$decimalValues,'decimal');
            $this->insertAttributes($connection,$textValues,'text');
            //configurable Relations
            $this->createConfiguration($connection, $data,$entities);
            $relationsArr = $this->parentChildRelation($connection, $data, $entities);
            $this->insertRelations($connection, $relationsArr);
            //Websites Linkage
            $this->insertWebsiteIds($connection, $data, $entities);
            $this->createStockEntries($connection, $data, $entities);
        }
        
        $response = "Import Successful";
        return $this->prepareResponse($response);
        
    }
    private function insertStoreStatus($connection,$data,$entities,$exisitngStores){
        $values = "";
        if(count($exisitngStores) > 0){
            foreach($data as $product){
                $entityId = array_search($product['sku'], $entities);
                if(isset($product['store_status'])){
                    foreach($exisitngStores as $storeid){
                        if(isset($product['store_status'][$storeid])){
                            $values .= ",(97,$storeid,$entityId,".$product['store_status'][$storeid].")";
                        }
                    }
                }
                if(isset($product['children']) && count($product['children']) > 0){
                    foreach($product['children'] as $child){
                        $entityIdChild = array_search($child['sku'], $entities);
                        if(isset($child['store_status'])){
                            foreach($exisitngStores as $storeid){
                                if(isset($child['store_status'][$storeid])){
                                    $values .= ",(97,$storeid,$entityIdChild,".$child['store_status'][$storeid].")";
                                }
                            }
                        }
                    }
                }
            }
            $query = "INSERT INTO catalog_product_entity_int(attribute_id,store_id,entity_id,value) VALUES ". ltrim($values, ',')." ON DUPLICATE KEY UPDATE attribute_id = VALUES(attribute_id), store_id = VALUES(store_id), entity_id = VALUES(entity_id), value = VALUES(value);";
            $connection->query($query);
        }
    }
    
    private function checkStore($connection, $data){
        $allStores = [];
        foreach($data as $product){
            if(isset($product['store_status'])){
                $stores = array_keys($product['store_status']);
                foreach($stores as $store){
                    $allStores[] = $store;
                }
            }
            if(isset($product['children']) && count($product['children']) > 0){
                foreach($product['children'] as $child){
                    if(isset($child['store_status'])){
                        $stores = array_keys($product['store_status']);
                        foreach($stores as $store){
                            $allStores[] = $store;
                        }
                    }
                }
            }
        }
        $response = [];
        if(count(array_unique($allStores)) > 0){
            $query = "select * from store where store_id in (".implode(',',array_unique($allStores)).")";
            $result = $connection->fetchAll($query);
            foreach($result as $store){
                $response[] = $store['store_id'];
            }
        }
        return count($response) > 0 ? $response: [];
    }
    private function createStockEntries($connection, $data, $entities){
        $cataloginventoryStockItemQuery = "INSERT INTO cataloginventory_stock_item (product_id, stock_id, qty, is_in_stock, manage_stock) VALUES ";
        $cataloginventoryStockStatusQuery = "INSERT INTO cataloginventory_stock_status (product_id, website_id, stock_id, qty, stock_status) VALUES ";
        $CSIQvalues = "";
        $CSSQvalues = "";
        
        foreach($data as $parent => $product){
            
            $parentEntityId = array_search($product['sku'], $entities);
            $CSIQvalues .= ",($parentEntityId, 1,1,1,1)";
            $CSSQvalues .= ",($parentEntityId, 0,1,1,1)";
            
            foreach($product['children'] as $child){
                $childEntityId = array_search($child['sku'], $entities);
                $CSIQvalues .= ",($childEntityId, 1, 1, 1, 1)";
                $CSSQvalues .= ",($parentEntityId, 0,1,1,1)";
            }
        }
        $cataloginventoryStockItemQuery .= ltrim($CSIQvalues, ','). " ON DUPLICATE KEY UPDATE product_id = VALUES(product_id), stock_id = VALUES(stock_id), qty = VALUES(qty), is_in_stock = VALUES(is_in_stock), manage_stock = VALUES(manage_stock);";
        $cataloginventoryStockStatusQuery .= ltrim($CSSQvalues, ',') . " ON DUPLICATE KEY UPDATE product_id = VALUES(product_id), website_id = VALUES(website_id), stock_id = VALUES(stock_id), qty = VALUES(qty), stock_status = VALUES(stock_status);";
        
        $connection->query($cataloginventoryStockItemQuery);
        $connection->query($cataloginventoryStockStatusQuery);
    }
    
    private function insertWebsiteIds($connection, $data, $entities){
        $query = "INSERT INTO catalog_product_website (product_id, website_id) VALUES ";
        $values = "";
        foreach($data as $parent => $product){
            $parentEntityId = array_search($product['sku'], $entities);
            if(count($product['website_ids']) > 0){
                foreach($product['website_ids'] as $websiteId){
                    if($websiteId !== "")
                        $values .= ",($parentEntityId, $websiteId)";
                }
            }
            foreach($product['children'] as $child){
                $childEntityId = array_search($child['sku'], $entities);
                if(count($child['website_ids']) > 0){
                    foreach($child['website_ids'] as $websiteId){
                        if($websiteId !== "")
                            $values .= ",($childEntityId, $websiteId)";
                    }
                }
            }
        }
        if($values !== "" ){
        $query .= ltrim($values, ','). " ON DUPLICATE KEY UPDATE product_id = VALUES(product_id), website_id = VALUES(website_id)";
        $connection->query($query);
        }
    }
    
    private function insertRelations($connection, $relationsArr){
        $queryCatalogProductRelation = "INSERT INTO catalog_product_relation (parent_id, child_id) VALUES ";
        $queryCatalogProductSuperLink = "INSERT INTO catalog_product_super_link (product_id, parent_id) VALUES ";
        $catalogProductRelationValues = "";
        $catalogProductSuperLinkValues = "";
        foreach($relationsArr as $parentId => $childArr){
            foreach ($childArr as $childEntityId){
                $catalogProductRelationValues .= ",($parentId,$childEntityId)";
                $catalogProductSuperLinkValues .= ",($childEntityId, $parentId)";
            }
        }
        
        $queryCatalogProductRelation .= ltrim($catalogProductRelationValues, ',')." ON DUPLICATE KEY UPDATE parent_id = VALUES(parent_id), child_id = VALUES(child_id)";
        $queryCatalogProductSuperLink .= ltrim($catalogProductSuperLinkValues, ',')." ON DUPLICATE KEY UPDATE product_id = VALUES(product_id), parent_id = VALUES(parent_id)";
        $connection->query($queryCatalogProductRelation);
        $connection->query($queryCatalogProductSuperLink);
    }
    
    private function createConfiguration($connection, $data, $entities){
        $query = "INSERT INTO catalog_product_super_attribute (product_id, attribute_id, position) VALUES ";
        $values = "";
        foreach($data as $parent => $product){
            $entityId = array_search($product['sku'], $entities);
            $configurableAttribute = explode(',', $product['configurable_attribute']);
            $i = 0;
            foreach($configurableAttribute as $attribute){
                $values .= ",($entityId, $attribute, $i)";    
                $i++;
            }
        }
        $query .= ltrim($values, ',');
        $query .= " ON DUPLICATE KEY UPDATE product_id = VALUES(product_id), attribute_id = VALUES(attribute_id), position = VALUES(position)";
        $connection->query($query);
    }
    
    private function parentChildRelation($connection, $data, $entities){
        $idRelationArr = [];
        foreach($data as $parent => $product){
            $parentEntityId = array_search($product['sku'], $entities);
            foreach($product['children'] as $child){
                $ChildeEntityId = array_search($child['sku'], $entities);
                $idRelationArr[$parentEntityId][] = $ChildeEntityId; 
            }
        }
        return $idRelationArr;
    }
    
    private function insertAttributes($connection,$values,$type){
        if($values !== ""){
            $query = "INSERT INTO catalog_product_entity_$type (attribute_id,store_id,entity_id,value) Values ".$values ." ON DUPLICATE KEY UPDATE attribute_id = VALUES(attribute_id), store_id = VALUES(store_id), entity_id = VALUES(entity_id), value = VALUES(value)";
            $connection->query($query);
        }
    }
    
    private function processAttributes($connection, $data, $entities, $type){
        $values = "";
        foreach ($data as $parent => $product){
            $entityId = array_search($product['sku'], $entities);
            foreach($product['custom_attributes'] as $attribute){
                if($attribute['type'] == $type && $attribute['value'] !== ""){
                    $values .= ",(".$attribute['attribute_id'].",0,$entityId,'".addslashes($attribute['value'])."')";
                }
            }
            foreach($product['children'] as $child ){
                $entityChildId = array_search($child['sku'], $entities);
                foreach($child['custom_attributes'] as $attribute){
                if($attribute['type'] == $type && $attribute['value'] !== ""){
                    $values .= ",(".$attribute['attribute_id'].",0,$entityChildId,'".addslashes($attribute['value'])."')";
                }
            }
            }
        }
        return $values;
    }
    
    private function checkExistence($connection, $allSkus){
        $skus = implode("','",$allSkus);
        $query = "select sku from catalog_product_entity where sku in ('".$skus."')";
        $response = $connection->fetchAll($query);
        $reformedExisting = [];
        foreach ($response as $sku){
            $reformedExisting[] = $sku['sku'];
        }
        return $existing;
    }
    
    private function getEntityIds($connection, $skus){
        $skus = implode("','",$skus);
        $query = "select sku,entity_id from catalog_product_entity where sku in ('".$skus."')";
        $response = $connection->fetchAll($query);
        $reformedResponse = [];
            if(count($response) > 0){
             foreach ($response as $sku){
                $reformedResponse[$sku['entity_id']] = $sku['sku'];
            }   
        }
        
        return $reformedResponse;
    }
    
    private function createEntites($connection, $data, $exisitingSku = [],$attributeSetId){
        $query = "INSERT INTO catalog_product_entity (attribute_set_id ,type_id,sku,has_options,required_options,created_at,updated_at) VALUES ";
        $values = "";
        foreach($data as $parent => $product){
            if(!in_array($parent, $exisitingSku))
                $values.=",($attributeSetId,'".$product['type_id']."','$parent',0,0,'".date('Y-m-d h:i:s')."','".date('Y-m-d h:i:s')."')";
            foreach($product['children'] as $child){
                if(!in_array($child['sku'], $exisitingSku))
                    $values.=",($attributeSetId,'".$child['type_id']."','".$child['sku']."',0,0,'".date('Y-m-d h:i:s')."','".date('Y-m-d h:i:s')."')";
            }
        }
        if($values !== ""){
            $query .= ltrim($values, ',');
            $connection->query($query);
        }
    }
    
    private function getAttributeInfo($connection , $attributeId){
        $query = "Select * from eav_attribute where attribute_id = $attributeId";
        $data = $connection->fetchAll($query);
        return $data[0];
    }
    
    private function prepareResponse($data , $error = false){
        
        if($data)
            return ['response' => $data, 'error' => $error];
        else
            return ['response' => false, 'error' => $error];
            
    }
    
    private function getEntities($connection , $skus){
        $skus = implode("','", $skus);
        $query = "Select entity_id,sku from catalog_product_entity where sku in ('$skus')";
        $data = $connection->fetchAll($query);
        $response = [];
        foreach($data as $entity){
            $response[$entity['sku']] = $entity['entity_id'];
        }
       return $response;
        
    }
    private function getSkuList($connection,$params){
        $query = "select sku from catalog_product_entity where type_id = 'simple' and attribute_set_id !=9;";
        $data = $connection->fetchAll($query);
        $response = [];
        foreach($data as $sku){
            $response[] = $sku['sku'];
        }
        return $this->prepareResponse($response);
    }
    
    private function getStatus($connection,$params){
        $response = [];
        $data = json_decode($params['data'],true);
        $skuString = implode("','", $data);
        $query = "select cpe.entity_id,cpe.sku,cpei.`store_id`,cpei.value as status from `catalog_product_entity_int`cpei  
        inner join catalog_product_entity cpe on cpei.entity_id = cpe.entity_id 
        where cpei.attribute_id = 97 and cpe.sku in ('$skuString')";
        $results = $connection->fetchAll($query);
        if(count($results) > 0){
            foreach ($results as $record){
                $response[$record['sku']][$record['store_id']] = $record['status'];
            }
            return $this->prepareResponse($response);
        }
        return $this->prepareResponse($response);
    }
    
    private function updateAttributes($connection,$params){
        $data = json_decode($params['data'],true);
        $queries = [];
        foreach($data as $attributeId => $attributeData){
            $skus = array_keys($attributeData['data']);
            $entity = $this->getEntities($connection, $skus);
            $q = "INSERT INTO catalog_product_entity_".$attributeData['type']." (attribute_id,store_id,entity_id,value) Values "; 
            $values = "";
            foreach($entity as $sku => $entity_id){
                $values .= ",(".$attributeData['attribute'].",0,$entity_id,".$attributeData['data'][$sku].")";
            }
            $q .= ltrim($values, ',');
            $q .= " ON DUPLICATE KEY UPDATE attribute_id = VALUES(attribute_id), store_id = VALUES(store_id), entity_id = VALUES(entity_id), value = VALUES(value)"; 
            $queries[] = $q;
        }
        foreach ($queries as $query){
            $connection->query($query);
        }
        $return  = "Products Updated";
        
        return $this->prepareResponse($return);
    }
}
